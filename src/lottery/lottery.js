import React from 'react';
import './lottery.css';

class Number extends React.Component {
	render() {
		return (
			<li className="number">{this.props.text}</li>
		)
	}
}

export default Number;