import React, {Component} from 'react';
import './App.css';
import Number from './lottery/lottery';


class App extends Component {
	
	state = {
		list: [],
		max: 36,
		min: 5,
		quantity: 5
	};
	
	
	getRandomNumbers = () => {
		let list = this.state.list;
		list = [];
		let counter = 0;
		while (list.length < this.state.quantity) {
			let number = Math.floor(Math.random() * (this.state.max - this.state.min + 1)) + this.state.min;
			let flag = true;
			for (let key of list) {
				if (number === key || number < key) {
					flag = false;
					break;
				}
			}
			if (number > (this.state.max + counter - this.state.quantity)) continue;
			if (flag) {
				list.push(number);
				counter++;
			}
		}
		this.setState({list});
	};
	
	render() {
		return (
			<div className="App">
				<header>
					<button onClick={this.getRandomNumbers}>New numbers</button>
				</header>
				<ul className="list">
					<Number text={this.state.list[0]}/>
					<Number text={this.state.list[1]}/>
					<Number text={this.state.list[2]}/>
					<Number text={this.state.list[3]}/>
					<Number text={this.state.list[4]}/>
				</ul>
			</div>
		);
	}
}

export default App;
